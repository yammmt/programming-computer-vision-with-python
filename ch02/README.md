- [VLFeat](http://www.vlfeat.org/) は現状最新の 0.9.21 のバイナリを引っ張ってきてもメモリエラーで sift が動作しない。しょうがないので 0.9.20 にすると動いた。
- Panoramio はサービス終了のため動作確認できず。
- Pydot 利用時に `neato is not found in path` で詰む。 brew で graphviz を入れる。

環境は mac OS High Sierra (10.13.4)
