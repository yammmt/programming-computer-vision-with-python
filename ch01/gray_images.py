from PIL import Image
from pylab import *
from numpy import *

im = array(Image.open('empire.jpg').convert('L'))
im2 = 255 - im
im3 = (100.0/255) * im + 100
im4 = 255.0 * (im/255.0)**2

images = (im, im2, im3, im4)

for im in images:
    print('min:{}, max:{}'.format(im.min(), im.max()))
    figure()
    gray()
    imshow(im)
show()
