import pdb
from PIL import Image
from scipy.ndimage import measurements, morphology
from numpy import *

im = array(Image.open('houses.png').convert('L'))
im = 1*(im<128)

labels, nbr_objects = measurements.label(im)
# pdb.set_trace()
print("Number of objects: ", nbr_objects)

im_open = morphology.binary_opening(im, ones((9, 5)), iterations=2)
labels_open, nbr_objects_open = measurements.label(im_open)
# pdb.set_trace()
print("Number of objects: ", nbr_objects_open)