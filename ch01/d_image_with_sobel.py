import pdb
from PIL import Image
from numpy import *
from pylab import *
from scipy.ndimage import filters

im = array(Image.open('empire.jpg').convert('L'))

imx = zeros(im.shape)
filters.sobel(im, 1, imx)

imy = zeros(im.shape)
filters.sobel(im, 0, imy)

magnitude = sqrt(imx**2 + imy**2)

images = (im, imx, imy, magnitude)

# pdb.set_trace()

figure()
gray()
i = 1
for i in range(len(images)):
    print(images[i].shape)
    subplot(1, 4, i + 1)
    axis('off')
    imshow(images[i])

show()