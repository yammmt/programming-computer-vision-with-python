import imregistration

# read control points
xmlFileName = 'jkfaces.xml'
points = imregistration.read_points_from_xml(xmlFileName)

# allign images
imregistration.rigid_alignment(points, './jkfaces/')
