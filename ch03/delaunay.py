from numpy import random
from numpy import *
import matplotlib.tri as tri
import matplotlib.pyplot as plt

x, y = array(random.standard_normal((2, 100)))
tri = tri.Triangulation(x, y).triangles

plt.figure()
for t in tri:
    t_ext = [t[0], t[1], t[2], t[0]]
    plt.plot(x[t_ext], y[t_ext], 'r')
plt.plot(x, y, '*')
plt.axis('off')
plt.show()
