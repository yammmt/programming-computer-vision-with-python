from numpy import *
from PIL import Image
from scipy import linalg
import os

def appendimages(im1, im2):
    rows1 = im1.shape[0]
    rows2 = im2.shape[0]

    if rows1 < rows2:
        im1 = concatenate((im1, zeros((rows2-rows1, im1.shape[1]))), axis=0)
    elif rows1 > rows2:
        im2 = concatenate((im2, zeros((rows1-rows2, im2.shape[1]))), axis=0)
    return concatenate((im1, im2), axis=1)

def match(desc1, desc2):
    desc1 = array([d/linalg.norm(d) for d in desc1])
    desc2 = array([d/linalg.norm(d) for d in desc2])
    dist_ratio = 0.6
    desc1_size = desc1.shape

    matchscores = zeros(desc1_size[0], 'int')
    desc2t = desc2.T

    for i in range(desc1_size[0]):
        dotprods = dot(desc1[i,:], desc2t)
        dotprods = 0.9999*dotprods
        indx = argsort(arccos(dotprods))

        if arccos(dotprods) [indx[0]] < dist_ratio * arccos(dotprods)[indx[1]]:
            matchscores[i] = int(indx[0])

    return matchscores

def match_twosided(desc1, desc2):
    matches_12 = match(desc1. desc2)
    matches_21 = match(desc2, desc1)

    ndx_12 = matches_12.nonzero()[0]

    for n in ndx_12:
        if matches_12[int(matches_12[n])] != n:
            matches_12[n] = 0

    return matches_12

def plot_features(im, locs, circle=False):
    def draw_circle(c, r):
        t = arange(0, 1.01, .01)*2*pi
        x = r*cos(t) + c[0]
        y = r*sin(t) + c[1]
        plot(x, y, 'b', linewidth=2)

    imshow(im)
    if circle:
        for p in locs:
            draw_circle(p[:2], p[2])
    else:
        plot(locs[:, 0], locs[:, 1], 'ob')
    axis('off')

def plot_matches(im1, im2, locs1, locs2, matchscores, show_below=True):
    im3 = appendimages(im1, im2)
    if show_below:
        im3 = vstack((im3, im3))

    imshow(im3)

    cols1 = im1.shape[1]
    for i, m in enumerate(matchscores):
        if m > 0: plot([locs1[i][0], locs2[m][0]+cols1], [locs1[i][1], locs2[m][1]], 'c')
    axis('off')

def process_image(imagename, resultname, params="--edge-thresh 10 --peak-thresh 5"):
    if imagename[-3:] != 'pgm':
        im = Image.open(imagename).convert('L')
        im.save('tmp.pgm')
        imagename = 'tmp.pgm'

    cmmd = str("sift " + imagename + " --output=" + resultname + " " + params)
    os.system(cmmd)
    print 'processed', imagename, 'to', resultname

def read_features_from_file(filename):
    f = loadtxt(filename)
    return f[:, :4], f[:, 4:]

def write_features_to_file(filename, locs, desc):
    savetxt(filename, hstack((locs, desc)))