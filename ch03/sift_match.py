import sift

featname = ['Univ'+str(i+1)+'.sift' for i in range(5)]
imname   = ['Univ'+str(i+1)+'.jpg' for i in range(5)]

l = {}
d = {}
for i in range(5):
    sift.process_image(imname[i], featname[i])
    l[i], d[i] = sift.read_features_from_file(featname[i])
    l[i][:, [0, 1]] = l[i][:, [1, 0]] # x, y = row, col

matches = {}
for i in range(4):
    matches[i] = sift.match(d[i+1], d[i])
