import imtools
import pickle
from numpy import array
from numpy import dot
from numpy import where
from PIL import Image
from pylab import *
from scipy.cluster.vq import *

# get image list
imlist = imtools.get_imlist('selected_fontimages/')
imnbr = len(imlist)

# load model file
with open('font_pca_modes.pkl', 'rb') as f:
    immean = pickle.load(f)
    V = pickle.load(f)

# make array to store flatten imgs
immatrix = array([array(Image.open(im)).flatten() for im in imlist], 'f')

# project chief ingredients up to 40th
immean = immean.flatten()
projected = array([dot(V[:40], immatrix[i]-immean) for i in range(imnbr)])

# k-means
projected = whiten(projected)
centroids, distortion = kmeans(projected, 4)

code, distance = vq(projected, centroids)

# plot clusters
for k in range(4):
    ind = where(code == k)[0]
    figure()
    gray()
    for i in range(minimum(len(ind), 40)):
        subplot(4, 10, i+1)
        imshow(immatrix[ind[i]].reshape((25, 25)))
        axis('off')
    show()
